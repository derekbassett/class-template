# Title

Duration: (up to 60 minutes)

Every SFS class is about a Tool or a Technique. Good titles that keep class plans on track are "A Gentle Hands-on Introduction to (something)" and "How To (something)"


## Talk

(25% of Duration)
 
Cover your informational Objectives here. (7/3/1)

- What is/are the PROBLEM/S? What problem does this solve? What discomfort caused someone to invent it?
- WHAT is this? What is the Tool or Technique you're teaching?
  * What does this do?
  * How does this work?
  * Why does this matter? (see PROBLEM)
- WHY or WHEN would one choose this? (Or: this, rather than that or the other thing)


## Demo

(25% of Duration)

- Cover 1-3 performance Objectives here, in one EXERCISE.
- Think about Pair and Share first, because you'll use the same EXERCISE for both sections. i.e. You'll have the Learners do during Pair what you do during Demo.
- Create a specific example of the most common use-case (problem) for the Technique or Tool. Try to make the solution pair well.
- Show HOW you use this to solve the problem.
- Explain WHY you use this this way.


## Pair & Share

(50% of Duration)

Step-wise instructions to the pair go here:
1. Setup the environment
2. Install the thing
3. Configure the thing
4. Test the thing
5. Raise both hands in the air and make a victorious noise

Teacher: When most pairs have finished, if there's time, lead a group Share.

Suggestions:
- "Billy, what is the most important thing you picked up out of this lesson?"
- "Kim, did you notice which package took longest to download?"
- "Jan and Remi: You guys got through this very quickly. Would you mind doing a show-and-tell of your solution?"
