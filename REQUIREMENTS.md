REPLACE ALL THIS WITH YOUR CONTENT

If participant learners can show up with empty hands and empty heads, just delete this file.

Otherwise:

What should participant learners bring to the class?

* hardware
  - laptop?
  - router?
  - server?
  - cables?
* software
  - ssh client?
  - web browser?
  - docker?
  - virtualbox?
  - git client (GitLab account)?

What should participant learners know before class?

* Linux? basic or intermediate?
* Networking? basic or intermediate?
* Is there a pre-requisite class?
